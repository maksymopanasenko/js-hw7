function filterBy(arr, dataType) {
    const types = ['null', 'undefined', 'string', 'object', 'number', 'bigint', 'symbol', 'boolean'];

    if (Array.isArray(arr) && arr.length !== 0 && types.includes(dataType) ) {

        if (dataType == 'null') {
            return arr.filter(item => item !== null)
        } else if (dataType == 'object') {
            return arr.filter(item => typeof item != dataType && typeof item != 'function' || item == null)
        } else {
            return arr.filter(item => typeof item != dataType)
        }

    } else {
        return new Error(`your array is empty or it's not array or '${dataType}' is not a data type`);
    }
}

const filteredArray = filterBy(['hello', 'world', 23, '23', null], 'string');

console.log(filteredArray);